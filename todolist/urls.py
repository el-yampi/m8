from django.urls import path

from . import views

urlpatterns = [
    path('', views.todolist, name='todolist'),
    path('tasks/<int:pk>/', views.tasks, name='tasks'),
    path('delete/<int:pk>/', views.delete, name='delete'),
    path('edit/<int:pk>/', views.edit, name='edit'),
    path('task/<int:pk>/', views.task, name='task'),
    path('update/<int:pk>/', views.update, name='update'),
    path('new/<int:pk>/', views.new, name='new'),
    path('create/<int:pk>/', views.create, name='create'),
    path('terminar/<int:pk>/', views.terminar, name='terminar'),
    path('login',views.login_yampi,name='login_yampi'),
    path('register',views.register_yampi,name='register_yampi'),
    path('logout',views.logout_yampi,name='logout_yampi'),
]