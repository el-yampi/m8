from django.contrib import admin

# Register your models here.
from .models import TodoList,Todo
admin.site.register(TodoList)
admin.site.register(Todo)