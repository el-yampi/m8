from django.shortcuts import render,redirect
from django.http import HttpResponse
from django.contrib.auth import authenticate, login
from django.contrib.auth import logout as do_logout
from django.contrib.auth.models import User
from .models import TodoList,Todo
from datetime import datetime
from django.contrib.auth.forms import UserCreationForm
# Create your views here.
def todolist(request, message=""):
    if request.user.is_authenticated:
        tdlist = TodoList.objects.filter(creator=request.user)
        return render(request,"todolist/todolist.html",{'tdlist':tdlist,'message':message})
    else:
        return render(request,"todolist/login.html")
    #return render(request,"todolist/todolist.html")

def login_yampi(request):
    if request.method == 'POST':
        username = request.POST['username']
        password = request.POST['password']
        user = authenticate(username=username, password=password)
        if user is not None:
            # Save session as cookie to login the user
            login(request, user)
            # Success, now let's login the user.
            return render(request, 'todolist/todolist.html')
        else:
            # Incorrect credentials, let's throw an error to the screen.
            return render(request, 'todolist/login.html', {'error_message': 'Incorrect username and / or password.'})
    else:
        # No post data availabe, let's just show the page to the user.
        return render(request, 'todolist/login.html')


def logout_yampi(request):
    do_logout(request)
    return redirect('/')

def tasks(request,pk=None):
    lista_tasks= Todo.objects.filter(todolist = TodoList.objects.get(pk=pk))
    return render(request,"todolist/tasks.html",{'tasks':lista_tasks,'todos':pk})



def task(request,pk=None):
    task= Todo.objects.get(pk=pk)
    return render(request,"todolist/task.html",{'task':task})

def new(request,pk=None):
    return render(request,
        'todolist/form.html',
        context={'action': "create/"+str(pk),
        'button': 'Create',
        'description': ""
        }
    )
def create(request,pk=None):
    user = User.objects.get(username=request.user)
    todos = TodoList.objects.get(pk=pk)
    description = request.POST.get("description",False)
    Todo.objects.create(description=description,creator=user,todolist=todos)
    return todolist(request, message="Tarea creada")
def delete(request,pk):
    t = Todo.objects.get(pk=pk)
    t.delete()
    return todolist(request, message="Tarea borrada")
def update(request,pk):
    todo = Todo.objects.get(pk=pk)
    todo.description = request.POST["description"]
    todo.text = request.POST["text"]
    todo.save()
    return todolist(request,message="tarea actualizada")
def edit(request,pk):
    todo = Todo.objects.get(pk=pk)
    return render(request,
        'todolist/form.html',
        context={'action': "update/"+str(pk),
        'button': 'Update',
        'description': todo.description,
        'text': todo.text
        }
    )
def terminar(request,pk):
    task = Todo.objects.get(pk=pk)
    if task.status:
        task.status = False
        task.save()
        msg = "tarea desactivada"
    else:
        task.status = True
        task.save()
        msg = "tarea activada"

    
    return todolist(request, message=msg)
        
def register_yampi(request):
    # Creamos el formulario de autenticación vacío
    form = UserCreationForm()
    if request.method == "POST":
        # Añadimos los datos recibidos al formulario
        form = UserCreationForm(data=request.POST)
        # Si el formulario es válido...
        if form.is_valid():

            # Creamos la nueva cuenta de usuario
            user = form.save()
            todos = TodoList.objects.create(title="mytodolist",creator=user)
            Todo.objects.create(description="my task",creator=user,todolist=todos)
            return redirect('/')
            # Si el usuario se crea correctamente 
            if user is not None:
                # Hacemos el login manualmente
                login_yampi(request)
                # Y le redireccionamos a la portada
                return redirect('/')

    # Si llegamos al final renderizamos el formulario
    return render(request, "todolist/register.html", {'form': form})