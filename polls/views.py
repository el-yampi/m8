from django.shortcuts import render
from polls.models import Question,Choice
# Create your views here.
from django.http import HttpResponse


def polls(request):
    #questions = Question.objects.all()
    questions = Question.objects.all()
    return render(request,"polls/polls.html",{'questions':questions})